package com.example.gtd.exception;

public class TodoNotFoundException extends Exception{
    public TodoNotFoundException() {
        super("Todo is not found!");
    }
}
