package com.example.gtd.mapper;

import com.example.gtd.entity.Todo;
import com.example.gtd.service.TodoService;
import com.example.gtd.tdo.TodoRequest;
import com.example.gtd.tdo.TodoResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

public class TodoMapper {


    public static TodoResponse getTodoResponses(Todo todo) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo, todoResponse);
        return todoResponse;
    }

    public static Todo getTodoRequest(TodoRequest todoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest,todo);
        return todo;
    }
}