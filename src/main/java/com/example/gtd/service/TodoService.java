package com.example.gtd.service;

import com.example.gtd.entity.Todo;
import com.example.gtd.exception.TodoNotFoundException;
import com.example.gtd.mapper.TodoMapper;
import com.example.gtd.repository.TodoRepository;
import com.example.gtd.tdo.TodoRequest;
import com.example.gtd.tdo.TodoResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import java.util.stream.Collectors;

import static com.example.gtd.mapper.TodoMapper.getTodoRequest;

@Service
public class TodoService {
    @Autowired
    private TodoRepository todoRepository;

    public List<TodoResponse> getAll(){
        return todoRepository.findAll().stream().map(item -> TodoMapper.getTodoResponses(item)).collect(Collectors.toList());

    }

    public TodoResponse addTodo(TodoRequest todoRequest) {
        Todo todo = getTodoRequest(todoRequest);
        Todo savedTodo = todoRepository.save(todo);
        return TodoMapper.getTodoResponses(savedTodo);
    }

    public TodoResponse updateTodo(Integer id,TodoRequest todoRequest) throws TodoNotFoundException {
        Todo todo = todoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
        if(todoRequest.getText() != null ){
            todo.setText(todoRequest.getText());
        }
        if(todoRequest.getDone()!=null){
            todo.setDone(todoRequest.getDone());
        }
        return TodoMapper.getTodoResponses(todoRepository.save(todo));
    }

    public void removeTodo(Integer id) {
        todoRepository.deleteById(id);
    }

    public TodoResponse getById(Integer id) throws TodoNotFoundException {
        return TodoMapper.getTodoResponses(todoRepository.findById(id).orElseThrow(TodoNotFoundException::new));
    }
}
