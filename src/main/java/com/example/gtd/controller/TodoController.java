package com.example.gtd.controller;


import com.example.gtd.exception.TodoNotFoundException;
import com.example.gtd.service.TodoService;
import com.example.gtd.tdo.TodoRequest;
import com.example.gtd.tdo.TodoResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    private TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponse> getAllTodos(){
        return todoService.getAll();

    }

    @GetMapping("/{id}")
    public TodoResponse getTodById(@PathVariable Integer id) throws TodoNotFoundException {
        return todoService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse addTodo(@RequestBody TodoRequest todoRequest){
        return todoService.addTodo(todoRequest);
    }

    @PutMapping("/{id}")
    public TodoResponse updateTodo(@PathVariable Integer id,@RequestBody TodoRequest todoRequest) throws TodoNotFoundException {
        return todoService.updateTodo(id,todoRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public  void deleteTodo(@PathVariable Integer id){
        todoService.removeTodo(id);
    }


}
