package com.example.gtd;
import com.example.gtd.entity.Todo;
import com.example.gtd.repository.TodoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.util.List;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    private MockMvc client;

    @Autowired
    private TodoRepository todoRepository;

    @Test
    void should_return_todoList_when_getAll_given_todos() throws Exception {
        //give

        Todo todo1 = new Todo("get up more early!",false);
        Todo todo2 = new Todo("go home more early!",false);
        todoRepository.save(todo1);
        todoRepository.save(todo2);

        //when   //then
        client.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(todo1.getText()))
                .andExpect(jsonPath("$[0].done").value(todo1.getDone()))
                .andExpect(jsonPath("$[1].text").value(todo2.getText()))
                .andExpect(jsonPath("$[1].done").value(todo2.getDone()));


    }

    @Test
    void should_add_a_todo_when_perform_addTodo_given_a_todo() throws Exception {
        //give
        Todo todo1 = new Todo("get up early!",false);
        ObjectMapper objectMapper = new ObjectMapper();

        //when   //then
        client.perform(MockMvcRequestBuilders.post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(todo1)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.text").value(todo1.getText()))
                .andExpect(jsonPath("$.done").value(todo1.getDone()));


    }

    @Test
    void should_update_a_todo_when_perform_updateTodo_given_a_todo() throws Exception {
        //give
        Todo todo = new Todo("get up early!",false);
        Todo todoUpdated = new Todo("get up more early!",true);
        ObjectMapper objectMapper = new ObjectMapper();
        todoRepository.save(todo);

        //when   //then
        client.perform(MockMvcRequestBuilders.put("/todos/{id}",todo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(todoUpdated)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.text").value(todoUpdated.getText()))
                .andExpect(jsonPath("$.done").value(todoUpdated.getDone()));


    }

    @Test
    void should_return_204_when_perform_delete_given_Todo() throws Exception {
        // given
        Todo insertedTodo = todoRepository.save(new Todo("get up early!",false));

        // when
        client.perform(MockMvcRequestBuilders.delete("/todos/{id}", insertedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        // should
        Assertions.assertNull(todoRepository.findById(insertedTodo.getId()).orElse(null));

    }

    @Test
    void should_return_todo_when_getById_given_todos() throws Exception {
        //give
        Todo todo1 = new Todo("get up early!",false);
        Todo todo2 = new Todo("go home early!",false);
        todoRepository.saveAll(List.of(todo1,todo2));

        //when    //then
        client.perform(MockMvcRequestBuilders.get("/todos/{id}",todo1.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value(todo1.getText()))
                .andExpect(jsonPath("$.done").value(todo1.getDone()));

    }

    @Test
    void should_return_404_when_perform_put_by_id_given_id_not_exist() throws Exception {
        // given
        Todo todo = new Todo("get up early!",false);
        ObjectMapper objectMapper = new ObjectMapper();

        // when
        client.perform(MockMvcRequestBuilders.put("/todos/999")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(todo)))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // should
    }


}
